export default function apprun($window, $rootScope,$state){
    //$state.go('home');
     $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams) {
       $rootScope.currentState = toState;
    });
    $rootScope.$on('$stateChangeError',
    function(event, toState, toParams, fromState, fromParams, options){
        event.preventDefault();
    });
     $rootScope.online = navigator.onLine;
     $window.addEventListener("offline", function () {
       $rootScope.$apply(function() {
         $rootScope.online = false;
       });
     }, false);
     $window.addEventListener("online", function () {
       $rootScope.$apply(function() {
         $rootScope.online = true;
       });
     }, false);
}
