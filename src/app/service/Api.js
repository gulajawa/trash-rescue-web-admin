/**
*Http Rest Api
*/
import {BASE_URL} from './../config';



class Api{
  constructor($http){
     this.$http = $http;
  }
  get(url,data,config){
    url = BASE_URL + url;
    return  this.$http.get(url,data);
  }
  post(url,data,config){
    url = BASE_URL + url;
    return  this.$http.post(url,data);
  }
  put(url,data,config){
    url = BASE_URL + url;
    return  this.$http.put(url,data);
  }
  del(url,data,config){
    url = BASE_URL + url;
    return  this.$http.delete(url,data);
  }

}

export default Api;
