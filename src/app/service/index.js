import angular from 'angular';

import Api from './Api';
import {STORE_TOKEN} from './../config';
const MODULE_NAME = "app.service";

let accessTokenHttpInterceptor = ($cookies,$injector,$q,$crypto) => ({
        request: function(config) {
            let todecrypt = localStorage.getItem(STORE_TOKEN);
            let token =  null

            if(todecrypt){
              token =  JSON.parse($crypto.decrypt(todecrypt));
            }


            if(token){
                config.headers['Authorization'] = 'Bearer ' +  token;
            }

            return config;
        }
});

let httpInterceptorRegistry = ($httpProvider) =>{
    $httpProvider.interceptors.push('accessTokenHttpInterceptor');
}



angular.module(MODULE_NAME,[])
  .config(['$httpProvider',httpInterceptorRegistry])
  .factory('accessTokenHttpInterceptor', ['$cookies','$injector','$q','$crypto',accessTokenHttpInterceptor])
  .service('Api',['$http',Api]);

export default MODULE_NAME;
