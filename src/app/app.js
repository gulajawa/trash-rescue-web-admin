import angular from 'angular';
//import css
import 'angular-material/angular-material.min.css';

import '../style/app.css';
import '../style/ngProgress.css';
import '../style/fonticon.css';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import 'angular-material-data-table/dist/md-data-table.css';

//depencency
import uirouter from 'angular-ui-router';
import ngMaterial from 'angular-material';
import ngCookies from 'angular-cookies';
import ngProgress from './util/ngProgress';
import dataTable from 'angular-material-data-table';
import ngclipboard from 'ngclipboard/dist/ngclipboard';
import ngMap from 'ngmap';

import 'chart.js';
//excel export

import ngChart from 'angular-chart.js';



//run
import apprun from './app.run';
//theme
import theme from './app.theme';
//router
import routes from './app.router';
//controller
import controller from './controller';
//directives
import directives from './directive';
//service
import services from './service';
//Util
import apputil from './util';


const MODULE_NAME = 'app';

angular.module(MODULE_NAME, [apputil,dataTable,ngclipboard,ngCookies,ngMaterial,ngProgress,uirouter,ngChart,services,controller,directives,ngMap])
  .config(['$stateProvider','$urlRouterProvider',routes])
  .config(['$mdThemingProvider',theme])
  .run(["$window", "$rootScope","$state",apprun]);

export default MODULE_NAME;
