import {STORE_TOKEN} from './config';


export default function routes($stateProvider,$urlRouterProvider) {
  $stateProvider

   .state('login', {
     url: '/login',
     template: require('./view/login.view.html'),
     controller: 'LoginController',
     controllerAs: 'login',
     data : {
       pageTitle : 'Masuk'
     },
     onEnter:['AppUtil',function(AppUtil){
        let $state = AppUtil.getState();
        let islogin = AppUtil.getStore(STORE_TOKEN);

        if(islogin ){
          $state.go('home');
        }
     }]
   })

   .state('home',{
     url:'/',
     template:require('./view/dashboard.view.html'),
     controller: 'DashController',
     controllerAs: 'dash',
     data : {
       pageTitle : 'Dashboard'
     },
     onEnter:['AppUtil',function(AppUtil){
        let $state = AppUtil.getState();
        let islogin = AppUtil.getStore(STORE_TOKEN);

        if(!islogin ){
          $state.go('login');
        }
     }]
   })

   .state('home.livetps',{
     url:'live/tps',
     template:require('./view/live/tps.live.html'),
     controller:'TPSLiveController',
     controllerAs: 'live',
     data :{
       pageTitle: 'Live TPS'
     }
   })

   .state('home.data',{
     url:'data',
     template:require('./view/data/data.dash.view.html'),
     controller: 'DataDashController',
     controllerAs: 'data',
     data:{
       pageTitle: 'Dashboard'
     }
   })

   .state('home.datatps',{
     url:'data/tps',
     template:require('./view/data/data.tps.view.html'),
     controller: 'DataTpsController',
     controllerAs: 'tps',
     data:{
       pageTitle: 'Tps'
     }
   })

   .state('home.detailtps',{
     url:'data/tps/{tpsid}',
     template:require('./view/data/detail.tps.view.html'),
     controller: 'DetailTpsController',
     controllerAs: 'detail',
     data:{
       pageTitle: 'Detail TPS'
     }
   })

   .state('home.datasensor',{
     url:'data/sensor',
     template:require('./view/data/data.sensor.view.html'),
     controller: 'DataSensorController',
     controllerAs: 'sensor',
     data:{
       pageTitle: 'Sensor'
     }
   })

   .state('home.datapetugas',{
     url:'data/petugas',
     template:require('./view/data/data.petugas.view.html'),
     controller: 'DataPetugasController',
     controllerAs: 'petugas',
     data:{
       pageTitle: 'Petugas'
     }
   })

      .state('home.statistik',{
          url:'data/statistik',
          template:require('./view/analytics/statistik.view.html'),
          controller: 'StatistikController',
          controllerAs: 'statistik',
          data:{
              pageTitle: 'Statistik Pengangkutan'
          }
      })

   .state('home.dataadmin',{
     url:'data/administrator',
     template:require('./view/data/data.admin.view.html'),
     controller:'DataAdminController',
     controllerAs:'admin',
     data:{
       pageTitle: 'Data Administrator'
     }
   });
  $urlRouterProvider.when('','/data');
  $urlRouterProvider.otherwise('/data');
}
