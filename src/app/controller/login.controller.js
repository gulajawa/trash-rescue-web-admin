/**
*LoginCotroller
*/

import {STORE_NAME,STORE_TOKEN} from './../config';

class LoginController{
   constructor(Api,AppUtil,$timeout){

     this.event = null;
     this.Api = Api;
     this.AppUtil = AppUtil;
     this.focus = AppUtil.getFocus();
     this.ToastUtil = AppUtil.getToastUtil();
     this.ErrorUtil = AppUtil.getErrorUtil();
     this.DialogUtil = AppUtil.getDialogUtil();
     this.$cookies = AppUtil.getCookies();
     this.$state = AppUtil.getState();
     this.user = {};
     this.progress = AppUtil.createProgress();
     this.tmpbool = {
       statuslog:false,
       showdialog:false
     };

     let islogin = AppUtil.getStore(STORE_TOKEN);

     if(islogin){
       $timeout(()=> this.$state.go('home'), 0);
     }

     this.handleSuccess = (response) =>{
       this.tmpbool.statuslog = false;
       if(response.data){
         if(response.data.error){
             if(this.tmpbool.showdialog){
                this.progress.reset();
                this.ErrorUtil.checkError(this.event,response.data.code);
             }
             return;
         }
         if(this.tmpbool.showdialog){
            this.progress.complete();
         }
         this.AppUtil.saveStore(STORE_NAME,response.data.data);
         this.AppUtil.saveStore(STORE_TOKEN,response.data.data.token);
         this.$state.go('home');

       }
     };
     this.handleError = (error)=>{
       this.tmpbool.statuslog = false;
       if(this.tmpbool.showdialog){
          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
       }
     };
   }

   enterLogin(event){
     this.event = event;
     if(!this.user.email){
       this.ToastUtil.showSimpleToast("Masukkan email atau username");
       return;
     }
     if(!this.user.password){
       this.ToastUtil.showSimpleToast("Masukkan kata sandi");
       return;
     }

     if(this.tmpbool.statuslog){
       return;
     }

     if(this.user.email && this.user.password){
       if(this.user.password.length < 4){
         this.ToastUtil.showSimpleToast("Kata sandi terlalu pendek");
         return;
       }
       let user = Object.assign({},this.user);
        this.tmpbool.statuslog = true;
        this.tmpbool.showdialog = true;
       if(!this.AppUtil.validateEmail(user.email)){
         user.username = user.email;
         delete user.email;
       }
       this.progress.start();
       this.Api.post('admin/auth',user).then(this.handleSuccess,this.handleError);
     }

   }

   checkOnChange(event){
     this.event = event;
     if(this.tmpbool.statuslog){
       return;
     }
     if(this.user.email && this.user.password){
       if(this.user.password.length < 4){
         return;
       }
       let user = Object.assign({},this.user);
       this.tmpbool.statuslog = true;
       this.tmpbool.showdialog = false;
       if(!this.AppUtil.validateEmail(user.email)){
         user.username = user.email;
         delete user.email;
       }
       this.Api.post('admin/auth',user).then(this.handleSuccess,this.handleError);
     }
   }

   doResetPassword(user){
     this.tmpbool.statuslog = true;
     this.tmpbool.showdialog = true;
     this.progress.start();
     this.Api.post('admin/forget',user).then((response)=>{
        this.tmpbool.statuslog = false;
       if(response.data){
         if(response.data.error){
             this.progress.reset();
             this.ErrorUtil.checkError(this.event,response.data.code);
             return;
         }
         this.progress.complete();
         let tmps = {
           title:"Berhasil",
           content:"Kata sandi berhasil direset,periksa alamat email anda untuk melanjutkan."
         };
         this.DialogUtil.showAlert(this.event,tmps);
       }
     },this.handleError);
   }

   showResetPassword(event){
     this.event = event;
     let respass = {
       title: "Atur ulang kata sandi",
       content : "Masukkan alamat email atau username anda,kata sandi akan dikirimkan melalui alamat email.",
       placeholder: "email atau username",
       label : "atur ulang kata sandi",
     };
     this.DialogUtil.showPrompt(event,respass).then((result)=>{
       let user = {};
       if(!result){
         return;
       }
       if(this.AppUtil.validateEmail(result)){
          user.email = result;
       }else{
         user.username = result;
       }
       this.doResetPassword(user);
     },(err)=>{});
   }

}


export default LoginController;
