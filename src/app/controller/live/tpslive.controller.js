import {GOOGLE_MAPS_API_KEY,YDISTANCE,CONTAINER_HEIGHT} from '../../config';

let that = null;

export default class TPSLiveController {
  constructor(AppUtil,Api,Socket,$scope,NgMap) {
    this.googlemaps = {
      url:"https://maps.googleapis.com/maps/api/js?key="+GOOGLE_MAPS_API_KEY
    };
    this.progress = AppUtil.createProgress();
    this.$state = AppUtil.getState();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();
    this.Api = Api;
    this.socket = Socket.getSocket();
    this.map = null;
    this.$scope = $scope;

    this.socket.on('tps',(data) => {
        //ngapain?
        this.refresh();
    });

    this.listtps =$scope.$parent.dash.dataloader.tps.data;
    this.admin = $scope.$parent.dash.admin;

    this.handleSuccess = (response) =>{
        if(response.data){
          if(response.data.error){
              this.progress.reset();
              this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
              return;
          }
          this.listtps =  response.data.data;
          if(this.$scope.$parent != null){
                  this.$scope.$parent.dash.dataloader.tps.data = response.data.data;
          }
        }
          this.progress.complete();
    };

    this.handleError = (error)=>{
        this.progress.reset();
        this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
    };

    if(!$scope.$parent.dash.dataloader.tps.status){
        this.progress.start();
      this.refresh();
      $scope.$parent.dash.dataloader.tps.status = true;
    }else{
      this.refresh();
    }

    //maps here
    let mapStyle = [
    {
       featureType: "road",
       elementType: "labels",
       stylers: [
         { visibility: "off" }
       ]
    }];



    NgMap.getMap().then((map) => {
     this.map = map;
     map.setOptions({styles: mapStyle});
    });

    this.detail = {
      style:{'margin-top':'20px','height':'80px'},
      percentage:'0%',
      name:'Tes',
      address:'tes'
    }

    that = this;

  }

  calculatePercentage(j1,j2){
    let totalxz = YDISTANCE + CONTAINER_HEIGHT;

    if(j1 >= totalxz ){
      j1 = totalxz;

    }
    if(j2 >= totalxz){
      j2 = totalxz;

    }

    let j1fix = j1 - YDISTANCE;
    let j2fix = j2 - YDISTANCE;

    let total = (j1fix + j2fix) / 2;
    console.log(total);
    let persentase = (total / CONTAINER_HEIGHT) * 100;

    let prct = 100 - parseInt(persentase);
    return prct;
  }



  showDetailTps(){
    let index = that.listtps.findIndex(item => item._id == this.id);
    let tps = that.listtps[index];
    that.detail.name = tps.name;
    that.detail.address = tps.address;
    that.detail.percentage = '0%';
    that.detail.container = tps.container;
    that.detail.style= {
      'margin-top':'0px','height':'0px'
    };

    if(tps.container.length>0){
      for (let i = 0; i < tps.container.length; i++) {
        if(tps.container[i].status == "PENUH"){
          that.detail.percentage = '100%';
          that.detail.style= {
            'margin-top':'0px','height':'100px'
          };
          break;
        }
        if(tps.container[i].sensor != null){
          if(tps.container[i].status == "KOSONG"){
            continue;
          }
          let pr = that.calculatePercentage(tps.container[i].sensor.jarak_sensor_1,tps.container[i].sensor.jarak_sensor_2);
          let hg = pr + 'px';
          let mt = (100-pr) + 'px';
          that.detail.percentage = pr + "%";
          that.detail.style= {
            'margin-top':mt,'height':hg
          };
          break;
        }
      }
    }

    this.map.showInfoWindow('tpsdetail',this.id);
  }


  refresh(){

      let params = {
          params:{

          }
      };

      if(this.admin.role && this.admin.role.toUpperCase() == "ADMIN"){
          params.params.wilayah = this.admin.wilayah._id;
      }
    this.Api.get('tps',params).then(this.handleSuccess,this.handleError);
  }

  showBounce(tps){
    let listmark = Object.entries(this.map.markers);
    let index = -1;

    for (var i = 0; i < listmark.length; i++) {
      if(listmark[i][1].id == tps._id){
        index = i;
        break;
      }
    }
    if(i == -1){
      console.log('kosong');
      return
    }

    let marker = listmark[index][1];
    marker.setAnimation(google.maps.Animation.BOUNCE);
    setTimeout(function () {
      marker.setAnimation(null);
    }, 750);

    that.detail.name = tps.name;
    that.detail.address = tps.address;
    that.detail.container = tps.container;
    that.detail.percentage = '0%';
    that.detail.style= {
      'margin-top':'0px','height':'0px'
    };

    if(tps.container.length>0){
      for (let i = 0; i < tps.container.length; i++) {
        if(tps.container[i].status == "PENUH"){
          that.detail.percentage = '100%';
          that.detail.style= {
            'margin-top':'0px','height':'100px'
          };
          break;
        }
        if(tps.container[i].sensor != null){
          if(tps.container[i].status == "KOSONG"){
            continue;
          }
          let pr = that.calculatePercentage(tps.container[i].sensor.jarak_sensor_1,tps.container[i].sensor.jarak_sensor_2);
          let hg = pr + 'px';
          let mt = (100-pr) + 'px';
          that.detail.percentage = pr + "%";
          that.detail.style= {
            'margin-top':mt,'height':hg
          };
          break;
        }
      }
    }

    this.map.showInfoWindow('tpsdetail',tps._id);

  }

  isHaveContainer(item){
      if(item.container.length>0){
        return true;
      }
      return false;
  }

  getAnimation(item){
    if(item.container.length>0){
      let penuh = false;
      for (let i = 0; i < item.container.length; i++) {
        if(item.container[i].status == "PENUH"){
          penuh = true;
          break;
        }
      }
      if(penuh){
          return google.maps.Animation.BOUNCE;
      }
    }
    return null;

  }


  checkStatus(item){
    if(item.container.length>0){
      for (let i = 0; i < item.container.length; i++) {
        if(item.container[i].status == "PENUH"){
          return "kontainer-penuh";
          break;
        }
        if(item.container[i].status == "HAMPIR_PENUH" || item.container[i].status == "SETENGAH_PENUH"){
            return "kontainer-setengah-penuh";
            break;
        }
        if(item.container[i].status == "TIDAK_DIKETAHUI" || item.container[i].status == "KOSONG"){
          return "kontainer-kosong";
          break;
        }
      }
    }
    return "kontainer-kosong";
  }


}
