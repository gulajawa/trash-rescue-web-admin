/**
 * Created by Aprilita on 4/26/2017.
 */

export default class StatistikController{
    constructor(AppUtil,Api,$scope,$timeout){
      this.AppUtil = AppUtil;
      this.Api = Api;
      this.$scope = $scope;
      this.$state = AppUtil.getState();
      this.progress = AppUtil.createProgress();
      this.ErrorUtil = AppUtil.getErrorUtil();
      this.ToastUtil = AppUtil.getToastUtil();
      this.$timeout = $timeout;

      this.listtps =$scope.$parent.dash.dataloader.tps.data;
      this.listStatistik = $scope.$parent.dash.dataloader.statistik.data;
      this.listDate = [];
      this.listMode = ["Bulanan","Harian"];
      this.selectedDate = null;
      this.selectedMode = $scope.$parent.dash.dataloader.statistik.mode;
      this.title = $scope.$parent.dash.dataloader.statistik.title;
      this.selectedstatistikobject = null;
      this.listDataPengangkutan = [];


      this.loader = {
          onProgress : true,
          success: false,
          showSelect:true
      };

      this.errormsg = {
          title: "",
          msg: ""
      };


      this.handleSuccess = (response) =>{
          this.progress.complete();
          if(response.data){
            if(response.data.error){
                this.progress.reset();
                this.ErrorUtil.checkError(null,response.data.code,this.ToastUtil);
                return;
            }
            this.listtps =  response.data.data;
            if(this.$scope.$parent != null){
                    this.$scope.$parent.dash.dataloader.tps.data = response.data.data;
            }
          }
      };

      this.handleError = (error)=>{
          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(null,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
      };

      if(!$scope.$parent.dash.dataloader.tps.status){
        this.progress.start();
        this.refresh();
        $scope.$parent.dash.dataloader.tps.status = true;
      }

      if(!$scope.$parent.dash.dataloader.statistik.status){
        this.fetchStatistik();
      }else{
         this.loader.onProgress = false;
         this.loader.success = true;
      }



        this.labels = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        this.series = ['Jumlah'];
        this.data = [
            [0,0,0,0,0,0,0,0,0,0,0,0]
        ];

        this.datasetOverride = [{ yAxisID: 'y-axis-1' }];
        this.options = {
            scales: {
                yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left'
                    }
                ]
            }
        };

        this.initDateList();
    }

    refresh(){
        let admin = this.$scope.$parent.dash.admin;
        let params = {
            params:{

            }
        };



        if(admin.role && admin.role.toUpperCase() == "ADMIN"){
            params.params.wilayah = admin.wilayah._id;
        }
      this.Api.get('tps',params).then(this.handleSuccess,this.handleError);
    }

    refreshSelectedTps(){
        if(this.$scope.$parent != null){
            let tps = this.$scope.$parent.dash.dataloader.statistik.tmp;
            if(tps == null){
                this.fetchStatistik();
            }else{
                this.fetchStatistikBy(tps);
            }
        }else{
            this.fetchStatistik();
        }
    }

    downloadCsvPengangkutan(){
        let filename = "tr_data_pengangkutan_"+this.selectedDate + "_";

        if(this.$scope.$parent != null){
            let tps = this.$scope.$parent.dash.dataloader.statistik.tmp;
            if(tps != null){
                filename += tps.name.replace(" ", "");
            }else{
                filename += "semua";
            }
        }

        this.$timeout(()=> {
            alasql('SELECT * INTO XLSX("' + filename + '.xlsx",{headers:true}) \
                        FROM HTML("#tabledatatoexportexcell",{headers:true})');
            this.loader.onProgress = false;
            this.loader.success = true;
        },10);
    }

    getDataPengangkutan(myparam){
        let param = {
            year:this.selectedDate
        };
        let url = "angkut";
        if(myparam != null){
            url = "angkut/get/by";
            param.tps = myparam;
        }
        this.loader.onProgress = true;
        this.loader.success = false;
        this.Api.get(url,{params:param}).then((response)=>{
            this.listDataPengangkutan  = response.data.data;
            if(this.listDataPengangkutan.length == 0){
                this.ToastUtil.showSimpleToast("Tidak ada data");
                return;
            }
            this.downloadCsvPengangkutan();
        },(error)=>{
            this.loader.onProgress = false;
            this.loader.success = true;
            this.ToastUtil.showSimpleToast("Gagal mengambil data");
        });
    }

    fetchPengangkutanData(){
        if(this.listStatistik.length == 0){
            this.ToastUtil.showSimpleToast("Tidak ada data");
            return;
        }

        if(this.$scope.$parent != null){
            let tps = this.$scope.$parent.dash.dataloader.statistik.tmp;
            if(tps != null){
                this.getDataPengangkutan(tps._id);
            }else{
                this.getDataPengangkutan(null);
            }
        }else{
            this.getDataPengangkutan(null);
        }
    }

    processSelectedYear(){
        this.data[0] = [0,0,0,0,0,0,0,0,0,0,0,0];

        let indxselected = this.listStatistik.findIndex(item => item._id == this.selectedDate );
        if(indxselected == -1){
            return;
        }
        this.selectedstatistikobject = this.listStatistik[indxselected];
        this.selectedstatistikobject.monthlyusage.forEach((item)=>{
           this.data[0][item.month-1] = item.count;
        });
    }

    processSelectedMode() {
        if (this.$scope.$parent != null) {
            this.$scope.$parent.dash.dataloader.statistik.mode = this.selectedMode;
        }
    }
    initDateList(){
        if(this.listStatistik.length > 0){
            this.loader.showSelect = true;

            //
            if(this.selectedMode == null){
                this.selectedMode = this.listMode[0];
            }


            this.listDate = [];
            this.listStatistik.forEach((item)=>{
                this.listDate.push(item._id);
            });

            let yearnow = new Date().getFullYear();
            let indx = this.listDate.indexOf(yearnow);
            if(indx == -1){
                this.selectedDate = this.listDate[0];
            }else{
                this.selectedDate = this.listDate[indx];
            }
            this.processSelectedYear();
        }else{
            this.data = [[0,0,0,0,0,0,0,0,0,0,0,0]];
            this.loader.showSelect = false;
            this.selectedstatistikobject = {
                monthlyusage : []
            };
        }
    }


    initDataStatistik(data){
        this.listStatistik = data;
        this.initDateList();
    }

    fetchStatistikBy(tps){
        this.loader.onProgress = true;
        this.loader.success = false;
        this.title = "TPS " + tps.name;
        if(this.$scope.$parent != null){
            this.$scope.$parent.dash.dataloader.statistik.title = this.title;
            this.$scope.$parent.dash.dataloader.statistik.tmp = tps;
        }

        this.Api.get('angkut/get/statistik',{params:{tps:tps._id}}).then((response)=>{
            if(response.data){
                if(response.data.error){
                    this.ErrorUtil.checkError(null,response.data.code,this.ToastUtil);
                    this.loader.onProgress = false;
                    this.loader.success = false;
                    this.errormsg.title = "GAGAL";
                    this.errormsg.msg = "Gagal mengambil data statistik";
                    return;
                }

                if(this.$scope.$parent != null){
                    this.$scope.$parent.dash.dataloader.statistik.data = response.data.data;
                    this.$scope.$parent.dash.dataloader.statistik.status = true;
                }
                this.initDataStatistik(response.data.data);
                this.loader.onProgress = false;
                this.loader.success = true;
            }else{
                this.loader.onProgress = false;
                this.loader.success = false;
                this.errormsg.title = "GAGAL";
                this.errormsg.msg = "Terjadi kesalahan yang tidak diketahui";
            }
        },(error)=>{
            this.loader.onProgress = false;
            this.loader.success = false;
            this.errormsg.title = "GAGAL";
            this.errormsg.msg = "Gagal mengambil data statistik, periksa kembali jaringan anda";
        });
    }


    fetchStatistik(){
        this.loader.onProgress = true;
        this.loader.success = false;
        this.title = "Semua TPS";
        if(this.$scope.$parent != null){
            this.$scope.$parent.dash.dataloader.statistik.title = this.title;
            this.$scope.$parent.dash.dataloader.statistik.tmp = null;
        }
        let admin = this.$scope.$parent.dash.admin;
        let params = {
            params:{

            }
        };

        if(admin.role && admin.role.toUpperCase() == "ADMIN"){
            params.params.wilayah = admin.wilayah._id;
        }

       this.Api.get('angkut/get/statistik',params).then((response)=>{
           if(response.data){
               if(response.data.error){
                   this.ErrorUtil.checkError(null,response.data.code,this.ToastUtil);
                   this.loader.onProgress = false;
                   this.loader.success = false;
                   this.errormsg.title = "GAGAL";
                   this.errormsg.msg = "Gagal mengambil data statistik";
                   return;
               }

               if(this.$scope.$parent != null){
                   this.$scope.$parent.dash.dataloader.statistik.data = response.data.data;
                   this.$scope.$parent.dash.dataloader.statistik.status = true;
               }
               this.initDataStatistik(response.data.data);
               this.loader.onProgress = false;
               this.loader.success = true;
           }else{
               this.loader.onProgress = false;
               this.loader.success = false;
               this.errormsg.title = "GAGAL";
               this.errormsg.msg = "Terjadi kesalahan yang tidak diketahui";
           }

       },(error)=>{
           this.loader.onProgress = false;
           this.loader.success = false;
           this.errormsg.title = "GAGAL";
           this.errormsg.msg = "Gagal mengambil data statistik, periksa kembali jaringan anda";
       });

    }



}
