export default class NewAdminController {
  constructor(AppUtil,Api,$rootScope) {
    this.event = null;
    this.AppUtil = AppUtil;
    this.Api = Api;
    this.focus = AppUtil.getFocus();
    this.ToastUtil = AppUtil.getToastUtil();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.$state = AppUtil.getState();
    this.baru = {};
    this.progress = AppUtil.createProgress();
    this.$rootScope = $rootScope;
    this.listWilayah = AppUtil.getVariable(0);
    this.listRole= ["Pilih Akses User","SUPERADMIN","ADMIN"];
    this.baru.role = this.listRole[0];

      this.autocomplate = {
          isDisabled:false,
          isNoCache:false,
          selectedItem:null,
          searchText:''
      };

    this.handleSuccess = (response) =>{
       if(response.data){

         if(response.data.error){
             this.progress.reset();
             this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
             return;
         }
        this.progress.complete();
        this.close();
        this.ToastUtil.showSimpleToast("Admin baru berhasil ditambahkan");
        this.$rootScope.$emit('dataemit:admin',response.data.data);
       }
    };

    this.handleError = (error) => {
          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
    }
  }
  close(){
    this.AppUtil.getDialogUtil().cancel();
  }

    onItemSelected(item){
        this.autocomplate.selectedItem = item;
    }

  addNew(event){
    this.event = event;
    if(!this.baru.email){
       this.ToastUtil.showSimpleToast("Masukkan email ");
       return;
    }
    if(!this.AppUtil.validateEmail(this.baru.email)){
      this.ToastUtil.showSimpleToast("Email tidak valid");
      return;
    }
    if(!this.baru.username){
      this.ToastUtil.showSimpleToast("Masukkan username");
      return;
    }
    if(this.baru.username.length < 6){
      this.ToastUtil.showSimpleToast("Username terlalu pendek");
      return;
    }
    if(!this.baru.password){
      this.ToastUtil.showSimpleToast("Masukkan password");
      return;
    }
    if(this.baru.password.length <6){
      this.ToastUtil.showSimpleToast("Kata sandi terlalu pendek");
      return;
    }
    if(this.baru.role == this.listRole[0]){
        this.ToastUtil.showSimpleToast("Pilih akses administrator");
        return;
    }

    if(this.baru.role == this.listRole[2]){
        if(!this.autocomplate.selectedItem){
            this.ToastUtil.showSimpleToast("Pilih wiyalah admin");
            return;
        }
        this.baru.wilayah = this.autocomplate.selectedItem._id;
    }


    this.progress.start();
    this.Api.post('admin/new',this.baru).then(this.handleSuccess,this.handleError);
  }
}
