
export default class DetailTpsController {
  constructor( $stateParams,AppUtil,Api,$filter,$timeout,$rootScope) {
    this.Api = Api;
    this.AppUtil = AppUtil;
    this.$filter = $filter;
    this.$timeout = $timeout;
    this.ToastUtil = AppUtil.getToastUtil();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.progress = AppUtil.createProgress();
    this.$stateParams = $stateParams;
    this.$state = AppUtil.getState();
    this.event = null;
    this.myedit = {};
    this.tps = null;
    this.edit = null;
    this.listcontainer = null;
    this.tmplistcontainer;
    this.errormsg = {
        title:'',
        msg:''
    };

    this.loader = {
      onProgress : true,
      success:false
    }

    $rootScope.$on('dataemit:tpskontainer', (event, data) => {
      this.refresh();
    });

    this.handleSuccess = (response) =>{
      if(response.data){
        this.loader.onProgress = false;
        if(response.data.error || !response.data.data){
             this.loader.success = false;
             this.errormsg.title = "404";
             this.errormsg.msg = "Gagal mengambil data, TPS tidak ditemukan.";
             return;
        }

        this.loader.success = true;
        this.tps = Object.assign({},response.data.data);
        this.listcontainer = this.tps.container;
        this.edit = Object.assign({},response.data.data);
        this.edit.latitude = this.edit.location[1];
        this.edit.longitude = this.edit.location[0];
      }
    }
    this.handleError = (error) =>{
        this.errormsg.title = "Error";
        this.errormsg.msg = "Gagal mengambil data, periksa jaringan anda";
        this.progress.complete();
        this.loader.onProgress = false;
        this.loader.success = false;
        this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
    }

    this.refresh();

    //table

    this.selected = [];
    this.limitOption = [5,10,15,20];

    this.filter = {
      options: {
        debounce: 500
      }
    };

    this.options = {
      rowSelection: true,
      multiSelect: true,
      autoSelect: false,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    };
    this.query = {
      filter:'',
      order: '',
      limit: 5,
      page: 1
    };


  }

  refresh(){
        this.Api.get('tps/'+this.$stateParams.tpsid).then(this.handleSuccess,this.handleError);
  }

  closeFilter(){
    this.query.filter = '';
    this.filter.show = !this.filter.show;
    this.tmplistcontainer = this.tps.container;
  }

  onSearchChange(search){
     this.tmplistcontainer= this.$filter('filter')(this.tps.container,search);
     this.promise = this.$timeout(()=>{

     },2000);
  }

  updateName(event){
    this.event = event;
    if(this.myedit.name == false){
      if(!this.edit.name ||  this.edit.name == this.tps.name ){
        this.edit.name = this.tps.name;
        return;
      }
      this.Api.put('tps/update',{id:this.tps._id,name:this.edit.name}).then(this.handleSuccess,this.handleError);
    }
  }

  updateLocation(event){

    this.event = event;

    if(!this.myedit.location){
      if(isNaN(this.edit.latitude) || isNaN(this.edit.latitude)){
        this.edit.latitude = this.tps.location[1];
        this.edit.longitude = this.tps.location[0];
        return;
      }
      if(this.edit.longitude == this.tps.location[0] && this.edit.latitude == this.tps.location[1]){
          this.edit.latitude = this.tps.location[1];
          this.edit.longitude = this.tps.location[0];
        return;
      }
      this.progress.start();
      this.Api.put('tps/update',{id:this.tps._id,latitude:this.edit.latitude,longitude:this.edit.longitude}).then((response)=>{
        this.progress.complete();
        if(response.data && !response.data.error){
          this.tps = Object.assign({},response.data.data);
          this.edit = Object.assign({},response.data.data);
          this.edit.latitude = this.edit.location[1];
          this.edit.longitude = this.edit.location[0];
        }
      },this.handleError);
    }
  }

  deleteSelectedKontainer(){
    if(this.selected.length>0){
        this.selected.forEach((item)=>{
            this.Api.del('tps/remove/container',{params:{id:this.tps._id,container_id:item._id}}).then((response)=>{
              this.refresh();
            },this.handleError);
        });

        this.selected = [];
    }
  }

  /*new tps*/
  openNewContainer(event){
    this.AppUtil.setVariable(3,this.tps._id);
    this.event = event;
    this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"NewContainerControler",controllerAs:"newcontainer",template:require('../../view/dialog/add-container.dialog.html')});
  }

}
