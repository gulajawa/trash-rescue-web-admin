import {STORE_NAME} from '../../config';

export default class DetailPetugasController {
  constructor(AppUtil,Api) {
    this.AppUtil = AppUtil;
    this.Api = Api;
    this.myedit = {};
    this.user = Object.assign({},AppUtil.getVariable(0));
    this.edit = Object.assign({},AppUtil.getVariable(0));
    this.admin = AppUtil.getStore(STORE_NAME);
    this.listWilayah = AppUtil.getVariable(2);
    this.listTps = AppUtil.getVariable(1);

    this.progress = AppUtil.createProgress();
    this.event = null;
    this.$state = AppUtil.getState();

    this.currentPage = "page1";

    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();

    this.listJob = this.getListJob();

    this.autocomplate = {
       isDisabled:false,
       isNoCache:false,
       selectedItem:null,
       searchText:''
    };


    this.handleSuccess = (response) => {
      if(response.data){
        if(response.data.error){
          this.progress.reset();
          this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
          return;
        }

        this.progress.complete();
        this.user.name = response.data.data.name;
        this.user.no_hp = response.data.data.no_hp;
        this.user.wilayah = response.data.data.wilayah;
        if(response.data.data.email){
          this.user.email = response.data.data.email;
        }

        this.edit = Object.assign({},this.user);
      }
    };

    this.handleError = (error) => {
          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
    }

    this.initWilayah();
  }

    initWilayah(){
        if(this.admin.role == "ADMIN"){
            this.user.disable = true;
        }
    }

  getListJob(){
    let list = [];
    for (var i = 0; i < this.user.job.length; i++) {
     for (var j = 0; j < this.user.tps.length; j++) {
          if(this.user.tps[j]._id == this.user.job[i].tps){
             list.push(this.user.tps[j]);
             break;
          }
      }
    }

    return list;
  }

  updateWilayah(event){
    this.event = event;
    if(!this.user.wilayah.name){
        this.ToastUtil.showSimpleToast("Pilih wilayah");
        return;
    }
      let index = this.listWilayah.findIndex(x => x.name==this.user.wilayah.name);
      let wilayah  = this.listWilayah[index]._id;
      this.Api.put('petugas/update/'+this.user._id,{wilayah:wilayah}).then(this.handleSuccess,this.handleError);
  }

  onItemSelected(item){
    this.autocomplate.selectedItem = item;
  }

  updateName(event){
    this.event = event;
    if(this.myedit.name == false){
      if(!this.edit.name ||  this.edit.name == this.user.name ){
        this.edit.name = this.user.name;
        return;
      }
      this.progress.start();
      this.Api.put('petugas/update/'+this.user._id,{name:this.edit.name}).then(this.handleSuccess,this.handleError);
    }
  }

  updatePhoneNumber(event){
    this.event = event;
    if(this.myedit.no_hp == false){
      if(!this.edit.no_hp ||  this.edit.no_hp == this.user.no_hp ){
        this.edit.name = this.user.name;
        return;
      }
      if(isNaN(this.edit.no_hp)){
        return;
      }
      if(this.edit.no_hp[0] == 0){
        this.edit.no_hp = '+62' + this.edit.no_hp.slice(1,this.edit.no_hp.length);
      }
      this.progress.start();
      this.Api.put('petugas/update/'+this.user._id,{no_hp:this.edit.no_hp}).then(this.handleSuccess,this.handleError);
    }
  }

  checkIsExist(id){
    let res = false;
    for (let i = 0; i < this.user.tps.length; i++) {
      if (this.user.tps[i]._id == id){
        res = true;
        break;
      }
    }
    return res;
  }

  tambahTps(){
    if(this.autocomplate.selectedItem){
      if(this.checkIsExist(this.autocomplate.selectedItem._id)){
        this.ToastUtil.showSimpleToast("Tps sudah ditambahkan");
      }else{
        this.Api.post('petugas/add/tps',{id:this.user._id,id_tps:this.autocomplate.selectedItem._id}).then((response)=>{
          if(response.data && !response.data.error){
            this.user.tps.push(this.autocomplate.selectedItem);
            this.ToastUtil.showSimpleToast("Tps berhasil ditambahkan");
          }
        },this.handleError);
      }
    }
  }

  hapusTps(id,index){
      this.Api.del('petugas/tps',{params:{id:this.user._id,id_tps:id}}).then((response)=>{
        this.ToastUtil.showSimpleToast("Tps berhasil dihapus");
        this.user.tps.splice(index,1);
      },this.handleError);
  }

  hapusTugas(id,index){
    let idx = this.user.job.findIndex(x => x.tps == id);
    if(idx != -1){
        let job_id = this.user.job[idx]._id;
        this.Api.del('petugas/job/delete',{params:{id_petugas:this.user._id,id_job:job_id}}).then((respose)=>{
            this.ToastUtil.showSimpleToast("Tugas berhasil dihapus");
            this.listJob.splice(index,1);
        },this.handleError);
    }

  }

  updateEmail(event){
    this.event = event;
    if(this.myedit.email == false){
      if( this.edit.email == this.user.email ){
        this.edit.email = this.user.email;
        return;
      }
      if(!this.edit.email){
        this.edit.email = this.user.email;
         this.ToastUtil.showSimpleToast("Email tidak valid");
        return;
      }
      if(!this.AppUtil.validateEmail(this.edit.email)){
        this.ToastUtil.showSimpleToast("Email tidak valid");
        return;
      }
      this.progress.start();
      this.Api.put('petugas/update/'+this.user._id,{email:this.edit.email}).then(this.handleSuccess,this.handleError);
    }
  }

  close(){
    this.AppUtil.getDialogUtil().cancel();
  }
}
