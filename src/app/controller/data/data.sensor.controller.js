import {TIME_INTERVAL_SENSOR} from '../../config';

export default class DataSensorController {
  constructor(Api,$filter,$timeout,AppUtil,$scope) {
    this.AppUtil = AppUtil;
    this.$scope = $scope;
    this.$filter = $filter;
    this.$timeout = $timeout;
    this.$state = AppUtil.getState();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();
    this.progress = AppUtil.createProgress();
    this.DialogUtil = AppUtil.getDialogUtil();
    this.Api = Api;
    this.selected = [];
    this.limitOption = [10,25,50,100];

    this.listWilayah = this.$scope.$parent.dash.dataloader.wilayah.data;
    this.listTps = this.$scope.$parent.dash.dataloader.tps.data ;

    this.filter = {
      options: {
        debounce: 500
      }
    };

    this.options = {
      rowSelection: true,
      multiSelect: true,
      autoSelect: false,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    };
    this.query = {
      filter:'',
      order: 'username',
      limit: 10,
      page: 1
    };
    this.listsensor = this.$scope.$parent.dash.dataloader.sensor.data;
    this.tmplistsensor ;

      this.handleSuccess = (response) =>{
        this.progress.complete();
        if(response.data){
          if(response.data.error){
              this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
              return;
          }
          if(this.event == "RESET"){
            return;
          }
          this.listsensor = this.$scope.$parent.dash.dataloader.sensor.data = response.data.data;
        }
      };
      this.handleError = (error)=>{
          this.progress.reset();
           this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
      };
      if(!$scope.$parent.dash.dataloader.sensor.status){
        this.refresh();
        $scope.$parent.dash.dataloader.sensor.status = true;
      }
      if(!$scope.$parent.dash.dataloader.wilayah.status){
          this.Api.get('wilayah').then((response)=>{
              if(response.data){
                  this.listWilayah = response.data.data;
                  $scope.$parent.dash.dataloader.wilayah.data = response.data.data;
                  $scope.$parent.dash.dataloader.wilayah.status = true;
              }
          },this.handleError);
      }
      if(!$scope.$parent.dash.dataloader.tps.status){
        this.fetchListTps();
      }
  }

  fetchListTps(){
      let admin = this.$scope.$parent.dash.admin;
      let params = {
          params:{

          }
      };

      if(admin.role && admin.role.toUpperCase() == "ADMIN"){
          params.params.wilayah = admin.wilayah._id;
      }



      this.Api.get('tps',params).then((response)=>{
        if(response.data){
           this.listTps = response.data.data;
           this.$scope.$parent.dash.dataloader.tps.data = response.data.data;
           this.$scope.$parent.dash.dataloader.tps.status = true;
        }
      },this.handleError);
  }

  refresh(event){
    this.progress.start();
    if(event){
         this.event = event;
    }
      let admin = this.$scope.$parent.dash.admin;
      let params = {
          params:{

          }
      };

      if(admin.role && admin.role.toUpperCase() == "ADMIN"){
          params.params.wilayah = admin.wilayah._id;
      }
    this.promise = this.Api.get('sensor',params).then(this.handleSuccess,this.handleError);
  }

  closeFilter(){
    this.query.filter = '';
    this.filter.show = !this.filter.show;
    this.tmplistsensor = this.listsensor;
  }

  onSearchChange(search){
     this.tmplistsensor= this.$filter('filter')(this.listsensor,search);
     this.promise = this.$timeout(()=>{

     },2000);
  }

  deleteSensor(event){
    this.event = event;
    if(this.selected.length>0){
      for (var i = 0; i < this.selected.length; i++) {
        this.Api.del('sensor',{params:{api_key:this.selected[i].api_key}}).then((response)=>{
          this.refresh();
        },this.handleError);
      }
      this.selected = [];
    }
  }

  openTambahSensor(event){
     this.event= event;
      if(!this.$scope.$parent.dash.dataloader.tps.status || !this.$scope.$parent.dash.dataloader.wilayah.status ){
          return;
      }
      this.AppUtil.setVariable(0,this.$scope.$parent.dash.dataloader.wilayah.data);
      this.AppUtil.setVariable(1,this.$scope.$parent.dash.dataloader.tps.data);
      this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"NewSensorController",controllerAs:"newsensor",template:require('../../view/dialog/add-sensor.dialog.html')});

  }

  updateSensorData(item){
    let body = {
      api_key : item.api_key
    };
    let index = this.listWilayah.findIndex(x => x.name==item.wilayah.name);
    body.wilayah = this.listWilayah[index]._id;
    this.Api.put('sensor/update',body).then((response)=>{
        this.ToastUtil.showSimpleToast("Data diperbaruhui");
        this.refresh();
    },this.handleError);
  }

  showTambahSensor(event){
    this.event = event;
    let config = {
      title: "Tambar sensor baru",
      content : "Masukan alamat lengkap sensor",
      placeholder: "Alamat",
      label : "tambah sensor baru",
    };
    this.DialogUtil.showPrompt(event,config).then((result)=>{
      if(result){
        this.progress.start();
        this.Api.post('sensor/new',{address:result}).then((response)=>{
          this.progress.complete();
          if(response.data.error){
              this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
              return;
          }
          this.ToastUtil.showSimpleToast("Sensor baru berhasil ditambahkan");
          this.$scope.$parent.dash.dataloader.sensor.data.push(response.data.data);
          this.listsensor =   this.$scope.$parent.dash.dataloader.sensor.data;
        },this.handleError);
      }
    },this.handleError);
  }


  checkSensorIsActive(date){
    let tc = new Date().getTime() - TIME_INTERVAL_SENSOR*60*1000;
    let st = new Date(date).getTime();
    if(st >= tc){
      return "AKTIF";
    }
    return "TIDAK AKTIF"
  }

}
