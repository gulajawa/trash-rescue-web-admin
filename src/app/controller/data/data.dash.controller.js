export default class DataDashController{
  constructor(AppUtil,Api,$scope) {
    this.AppUtil = AppUtil;
    this.Api = Api;
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();
    this.$state = AppUtil.getState();
    this.$scope= $scope;
    this.progress = AppUtil.createProgress();
    this.dashboard  =  $scope.$parent.dash.dataloader.dash.data;

    this.handleSuccess = (response) =>{
      if(response.data.data){
        this.progress.complete();
        this.dashboard = response.data.data;
        $scope.$parent.dash.dataloader.dash.data = response.data.data;
      }

    };
    this.handleError = (error)=>{
        this.progress.reset();
        this.ErrorUtil.checkErrorByCodeNumber(null,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
    };
    this.refresh(null);
  }

  refresh(event){
    if(event){
         this.event = event;
    }
      let admin = this.$scope.$parent.dash.admin;
      let params = {
          params:{

          }
      };

      if(admin.role && admin.role.toUpperCase() == "ADMIN"){
          params.params.wilayah = admin.wilayah._id;
      }
    this.progress.start();
    this.Api.get('dashboard',params).then(this.handleSuccess,this.handleError);
  }

}
