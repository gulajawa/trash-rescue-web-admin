import {STORE_NAME} from '../../config';


export default class NewTpsController {
  constructor(AppUtil,Api,$rootScope) {
    this.event = null;
    this.AppUtil = AppUtil;
    this.Api = Api;
    this.focus = AppUtil.getFocus();
    this.ToastUtil = AppUtil.getToastUtil();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.$state = AppUtil.getState();
    this.baru = {};
    this.progress = AppUtil.createProgress();
    this.$rootScope = $rootScope;
    this.listWilayah = AppUtil.getVariable(0);
      this.user = AppUtil.getStore(STORE_NAME);

    this.handleSuccess = (response) =>{
       if(response.data){

         if(response.data.error){
             this.progress.reset();
             this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
             return;
         }
        this.progress.complete();
        this.close();
        this.ToastUtil.showSimpleToast("Tps baru berhasil ditambahkan");
        this.$rootScope.$emit('dataemit:tps',response.data.data);
       }
    };

    this.handleError = (error) => {
          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
    };

    this.initWilayah();
  }

    initWilayah(){
        if(this.user.role && this.user.role == "ADMIN"){
            let index = this.listWilayah.findIndex(x => x.name==this.user.wilayah.name);
            this.baru.namawilayah = this.listWilayah[index].name;
            this.baru.disable = true;
        }
    }

  close(){
    this.AppUtil.getDialogUtil().cancel();
  }


  addNew(event){
    this.event = event;
    if(!this.baru.name){
       this.ToastUtil.showSimpleToast("Masukkan nama tps");
       return;
    }

    if(!this.baru.latitude){
      this.ToastUtil.showSimpleToast("Masukkan latitude lokasi tps");
      return;
    }

    if(!this.baru.longitude){
      this.ToastUtil.showSimpleToast("Masukkan latitude lokasi tps");
      return;
    }

    if(!this.baru.container){
      this.ToastUtil.showSimpleToast("Masukkan jumlah kontainer");
      return;
    }
    if(!this.baru.namawilayah){
        this.ToastUtil.showSimpleToast("Masukkan wilayah");
        return;
    }


    this.progress.start();
    let tpsbaru = Object.assign({},this.baru);
      let index = this.listWilayah.findIndex(x => x.name==this.baru.namawilayah);
      tpsbaru.wilayah = this.listWilayah[index]._id;
    this.Api.post('tps/new',tpsbaru).then(this.handleSuccess,this.handleError);
  }

}
