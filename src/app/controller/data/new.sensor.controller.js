/**
 * Created by muharizals on 13/05/2017.
 */
import {STORE_NAME} from '../../config';

export default class NewSensorController{
    constructor(AppUtil,Api,$rootScope){
        this.AppUtil = AppUtil;
        this.Api = Api;
        this.ToastUtil = AppUtil.getToastUtil();
        this.$rootScope = $rootScope;
        this.user = AppUtil.getStore(STORE_NAME);
        this.listWilayah = AppUtil.getVariable(0);
        this.listTps = AppUtil.getVariable(1);
        this.baru = {};
        this.initWilayah();
        this.progress = AppUtil.createProgress();

        this.autocomplate = {
            isDisabled:false,
            isNoCache:false,
            selectedItem:null,
            searchText:''
        };

        this.handleError = (error) => {
            this.progress.reset();
            this.ToastUtil.showSimpleToast("Terjadi kesalahan");
        }
    }

    onItemSelected(item){
        this.autocomplate.selectedItem = item;
    }

    close(){
        this.AppUtil.getDialogUtil().cancel();
    }

    addNew(){
        if(!this.autocomplate.selectedItem){
            this.ToastUtil.showSimpleToast("Pilih tps lokasi sensor");
            return;
        }
        if(!this.baru.namawilayah){
            this.ToastUtil.showSimpleToast("Pilih wilayah");
            return;
        }
        let body = {
            tps_id : this.autocomplate.selectedItem._id
        };
        let index = this.listWilayah.findIndex(x => x.name==this.baru.namawilayah);
        body.wilayah = this.listWilayah[index]._id;
        this.progress.start();
        console.log(body);
        this.Api.post('sensor/new',body).then((response)=>{
            this.progress.complete();
            this.close();
            this.ToastUtil.showSimpleToast("Sensor baru berhasil ditambahkan");
            this.$rootScope.$emit('dataemit:sensor',response.data.data);
        },this.handleError);
    }

    initWilayah(){
        if(this.user.role && this.user.role == "ADMIN"){
            let index = this.listWilayah.findIndex(x => x.name==this.user.wilayah.name);
            this.baru.namawilayah = this.listWilayah[index].name;
            this.baru.disable = true;
        }
    }
}