export default class NewContainerControler {
  constructor(AppUtil,Api,$rootScope) {
    this.event = null;
    this.AppUtil = AppUtil;
    this.Api = Api;
    this.focus = AppUtil.getFocus();
    this.ToastUtil = AppUtil.getToastUtil();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.$state = AppUtil.getState();
    this.baru = {};
    this.baru.id = AppUtil.getVariable(3);
    this.progress = AppUtil.createProgress();
    this.$rootScope = $rootScope;

    this.handleSuccess = (response) =>{ 
       if(response.data){
         if(response.data.error){
             this.progress.reset();
             this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
             return;
         }
        this.progress.complete();
        this.close();
        this.ToastUtil.showSimpleToast("kontainer baru berhasil ditambahkan");
         this.$rootScope.$emit('dataemit:tpskontainer',response.data.data.container);
       }
    }

    this.handleError = (error) => {
          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
    }

  }

  close(){
    this.AppUtil.getDialogUtil().cancel();
  }

  addNew(event){
    this.event = event;
    if(!this.baru.jumlah){
       this.ToastUtil.showSimpleToast("Masukkan jumlah kontainer");
       return;
    }

    this.progress.start();
    let kontainerbaru = Object.assign({},this.baru);
    this.Api.post('tps/add/container',kontainerbaru).then(this.handleSuccess,this.handleError);
  }

}
