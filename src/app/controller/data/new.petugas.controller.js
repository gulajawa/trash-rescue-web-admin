import {STORE_NAME} from '../../config';


export default class NewPetugasController{
  constructor(AppUtil,Api,$rootScope) {
    this.event = null;
    this.AppUtil = AppUtil;
    this.Api = Api;
    this.focus = AppUtil.getFocus();
    this.ToastUtil = AppUtil.getToastUtil();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.$state = AppUtil.getState();
    this.baru = {};
    this.progress = AppUtil.createProgress();
    this.$rootScope = $rootScope;
    this.user = AppUtil.getStore(STORE_NAME);

    this.listWilayah = AppUtil.getVariable(0);

      this.autocomplate = {
          isDisabled:false,
          isNoCache:false,
          selectedItem:null,
          searchText:''
      };

    this.handleSuccess = (response) =>{
       if(response.data){

         if(response.data.error){
             this.progress.reset();
             this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
             return;
         }
        this.progress.complete();
        this.close();
        this.ToastUtil.showSimpleToast("Petugas baru berhasil ditambahkan");
        this.$rootScope.$emit('dataemit:petugas',response.data.data);
       }
    };

    this.handleError = (error) => {

          this.progress.reset();
          this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
    };

    this.initWilayah();
  }

  initWilayah(){
      if(this.user.role && this.user.role == "ADMIN"){
          let index = this.listWilayah.findIndex(x => x.name==this.user.wilayah.name);
          this.baru.namawilayah = this.listWilayah[index].name;
          this.baru.disable = true;
      }
  }


  close(){
    this.AppUtil.getDialogUtil().cancel();
  }

  addNew(event){
    this.event = event;
    if(!this.baru.name){
       this.ToastUtil.showSimpleToast("Masukkan nama petugas");
       return;
    }

    if(!this.baru.no_hp || isNaN(this.baru.no_hp)){
      this.ToastUtil.showSimpleToast("Masukkan nomor hp ");
      return;
    }

    if(!this.baru.namawilayah ){
        this.ToastUtil.showSimpleToast("Pilih wilayah");
        return;
    }

    if(this.baru.no_hp[0] == 0){
        this.baru.no_hp = '+62' + this.baru.no_hp.slice(1,this.baru.no_hp.length);
    }

    let index = this.listWilayah.findIndex(x => x.name==this.baru.namawilayah);
    this.baru.wilayah = this.listWilayah[index]._id;

    this.progress.start();
    let petugasbaru = Object.assign({},this.baru);
    this.Api.post('petugas/new',petugasbaru).then(this.handleSuccess,this.handleError);
  }
}
