export default class DataTpsController {
  constructor(Api,$filter,$timeout,AppUtil,$scope) {
    this.AppUtil = AppUtil;
    this.$filter = $filter;
    this.$timeout = $timeout;
    this.$scope = $scope;
    this.$state = AppUtil.getState();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();
    this.progress = AppUtil.createProgress();
    this.event = null;

    this.listWilayah = this.$scope.$parent.dash.dataloader.wilayah.data;

    this.Api = Api;
    this.selected = [];
    this.limitOption = [10,25,50,100];

    this.filter = {
      options: {
        debounce: 500
      }
    };

    this.options = {
      rowSelection: true,
      multiSelect: true,
      autoSelect: false,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    };
    this.query = {
      filter:'',
      order: 'username',
      limit: 10,
      page: 1
    };
    this.listtps =this.$scope.$parent.dash.dataloader.tps.data;
    this.tmplisttps ;

    this.handleSuccess = (response) =>{
        this.progress.complete();
      if(response.data){
        if(response.data.error){
            this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
            return;
        }
        if(this.event == "RESET" || this.event == "DELETEALL"){
          return;
        }
        if(this.event == "DELETE"){
          this.listtps.splice(this.id,1);
          this.$scope.$parent.dash.dataloader.tps.data = this.listtps;
          this.ToastUtil.showSimpleToast("Tps berhasil dihapus");
          return;
        }
        this.listtps = this.$scope.$parent.dash.dataloader.tps.data = response.data.data;
      }
    };
    this.handleError = (error)=>{
        this.progress.reset();
         this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
    };
    if(!$scope.$parent.dash.dataloader.tps.status){
      this.refresh();
      $scope.$parent.dash.dataloader.tps.status = true;
    }
    if(!$scope.$parent.dash.dataloader.wilayah.status){
        this.Api.get('wilayah').then((response)=>{
            if(response.data){
                this.listWilayah = response.data.data;
                $scope.$parent.dash.dataloader.wilayah.data = response.data.data;
                $scope.$parent.dash.dataloader.wilayah.status = true;
            }
        },this.handleError);
    }

  }

  updateTPSData(item){
      let body = {
          id : item._id
      };
      let index = this.listWilayah.findIndex(x => x.name==item.wilayah.name);
      body.wilayah = this.listWilayah[index]._id;
      this.Api.put('tps/update',body).then((response)=>{
          this.ToastUtil.showSimpleToast("Data diperbaruhui");
          this.refresh();
      },this.handleError);
  }

  refresh(event){
    if(event){
         this.event = event;
    }
      let admin = this.$scope.$parent.dash.admin;
      let params = {
          params:{

          }
      };

      if(admin.role && admin.role.toUpperCase() == "ADMIN"){
          params.params.wilayah = admin.wilayah._id;
      }
    this.progress.start();
    this.promise = this.Api.get('tps',params).then(this.handleSuccess,this.handleError);
  }

  closeFilter(){
    this.query.filter = '';
    this.filter.show = !this.filter.show;
    this.tmplisttps = this.listtps;
  }

  onSearchChange(search){
     this.tmplisttps= this.$filter('filter')(this.listtps,search);
     this.promise = this.$timeout(()=>{

     },2000);
  }

  deleteTps(id,index){
    this.id = index;
    this.event = "DELETE";
    this.Api.del('tps/remove',{params:{id:id}}).then(this.handleSuccess,this.handleError);
  }

  deleteSelectedTps(){
    if(this.selected.length>0){
        this.event = "DELETEALL";
        this.selected.forEach((item)=>{
            this.Api.del('tps/remove',{params:{id:item._id}}).then((response)=>{
            for(let i = 0;i<this.listtps.length;i++){
              if(this.listtps[i]._id == item._id){
                this.listtps.splice(i,1);
                this.$scope.$parent.dash.dataloader.tps.data = this.listtps;
                break;
              }
            }
          },this.handleError);
        });
        this.ToastUtil.showSimpleToast("Tps berhasil dihapus");
        this.selected = [];
    }
  }


  openNewTps(event){
    this.event = event;
    this.AppUtil.setVariable(0,this.$scope.$parent.dash.dataloader.wilayah.data);
    this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"NewTpsController",controllerAs:"newtps",template:require('../../view/dialog/add-tps.dialog.html')});
  }


}
