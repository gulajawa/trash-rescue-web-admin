import {STORE_NAME} from '../../config';

export default class DataAdminController{
  constructor(Api,$filter,$timeout,AppUtil,$scope) {
    this.event;
    this.$scope = $scope;
    this.AppUtil = AppUtil;
    this.$filter = $filter;
    this.$timeout = $timeout;
     this.$state = AppUtil.getState();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();
     this.progress = AppUtil.createProgress();
      this.user = AppUtil.getStore(STORE_NAME);
    this.Api = Api;
    this.selected = [];
    this.limitOption = [10,25,50,100];
    this.listRole= ["SUPERADMIN","ADMIN"];
    this.listWilayah = this.$scope.$parent.dash.dataloader.wilayah.data;

    this.filter = {
      options: {
        debounce: 500
      }
    };

    this.options = {
      rowSelection: true,
      multiSelect: true,
      autoSelect: false,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    };
    this.query = {
      filter:'',
      order: 'username',
      limit: 10,
      page: 1
    };

    this.tmplistadmin ;

    this.listAdmin =   this.$scope.$parent.dash.dataloader.admin.data;

    this.handleSuccess = (response) =>{
        this.progress.complete();
      if(response.data){
        if(response.data.error){
            this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
            return;
        }
        if(this.event == "RESET"){
          return;
        }
        this.listAdmin = this.$scope.$parent.dash.dataloader.admin.data = response.data.data;

      }
    };
    this.handleError = (error)=>{
         this.progress.reset();
         this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
    };

    if(!$scope.$parent.dash.dataloader.admin.status){
      this.refresh();
      $scope.$parent.dash.dataloader.admin.status = true;
    }
    if(!$scope.$parent.dash.dataloader.wilayah.status){
      this.Api.get('wilayah').then((response)=>{
        if(response.data){
          this.listWilayah = response.data.data;
          $scope.$parent.dash.dataloader.wilayah.data = response.data.data;
            $scope.$parent.dash.dataloader.wilayah.status = true;
        }
      },this.handleError);
    }

  }

  refresh(event){
    if(event){
         this.event = event;
    }
    this.progress.start();
    this.promise = this.Api.get('admin').then(this.handleSuccess,this.handleError);
  }


  deleteAdministrator(event){
    this.event = event;
    if(this.selected.length>0){
      this.selected.forEach((elem)=>{
        if(elem.username != this.user.username){
          this.Api.del('admin/delete/single',{params:{id:elem._id}}).then((response)=>{
            this.refresh();
          },this.handleError);
        }
      });
      this.selected = [];
    }
  }

  resetPassword(){
    this.event = "RESET";
    if(this.selected.length>0){
      this.selected.forEach((elem)=>{
        if(elem.email){
          this.progress.start();
          this.Api.post('admin/forget',{email:elem.email}).then(this.handleSuccess,this.handleError);
        }
      });
    }
  }

  openNewAdmin(event){
    this.AppUtil.setVariable(0,this.$scope.$parent.dash.dataloader.wilayah.data);
    this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"NewAdminController",controllerAs:"newadmin",template:require('../../view/dialog/add-admin.dialog.html')});
  }

  closeFilter(){
    this.query.filter = '';
    this.filter.show = !this.filter.show;
    this.tmplistadmin = this.listAdmin;
  }


  onSearchChange(search){
     this.tmplistadmin= this.$filter('filter')(this.listAdmin,search);
     this.promise = this.$timeout(()=>{

     },2000);
  }

  updateAdminData(item){
    if(item.username == this.user.username){
      return;
    }

    let body ={
      id:item._id,
      role:item.role
    };
    if(item.role == this.listRole[1]){
      if(!item.wilayah.name){
        return;
      }
      let index = this.listWilayah.findIndex(x => x.name==item.wilayah.name);
      body.wilayah = this.listWilayah[index]._id;
    }
    this.Api.put('admin/update',body).then((response)=>{
      this.refresh();
      this.ToastUtil.showSimpleToast("Data diperbaruhui");
    },this.handleError);
  }

}
