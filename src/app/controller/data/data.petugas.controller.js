export default class DataPetugasController {
  constructor(Api,$filter,$timeout,AppUtil,$scope) {
    this.AppUtil = AppUtil;
    this.$scope = $scope;
    this.$filter = $filter;
    this.$timeout = $timeout;
    this.$state = AppUtil.getState();
    this.ErrorUtil = AppUtil.getErrorUtil();
    this.ToastUtil = AppUtil.getToastUtil();
    this.progress = AppUtil.createProgress();
    this.id = null;
    this.Api = Api;
    this.selected = [];
    this.limitOption = [10,25,50,100];

    this.filter = {
      options: {
        debounce: 500
      }
    };

    this.options = {
      rowSelection: true,
      multiSelect: true,
      autoSelect: false,
      decapitate: false,
      largeEditDialog: false,
      boundaryLinks: false,
      limitSelect: true,
      pageSelect: true
    };
    this.query = {
      filter:'',
      order: 'username',
      limit: 10,
      page: 1
    };
    this.listpetugas = this.$scope.$parent.dash.dataloader.petugas.data;
    this.tmplistpetugas ;


      this.handleSuccess = (response) =>{
          this.progress.complete();
        if(response.data){
          if(response.data.error){
              this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
              return;
          }
          if(this.event == "RESET" || this.event == "DELETEALL"){
            return;
          }
          if(this.event == "DELETE"){
            this.ToastUtil.showSimpleToast("Petugas berhasil dihapus");
            for(let i = 0;i<this.listpetugas.length;i++){
              if(this.listpetugas[i]._id == this.id){
                this.listpetugas.splice(i,1);
                this.$scope.$parent.dash.dataloader.petugas.data = this.listpetugas;
                break;
              }
            }

            return;
          }
          this.listpetugas = this.$scope.$parent.dash.dataloader.petugas.data = response.data.data;
        }
      };
      this.handleError = (error)=>{
          this.progress.reset();
           this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage,this.ToastUtil);
      };

      if(!$scope.$parent.dash.dataloader.petugas.status){
        this.refresh();
        $scope.$parent.dash.dataloader.petugas.status = true;
      }
  }

  refresh(event){
    if(event){
         this.event = event;
    }
    this.progress.start();
    let admin = this.$scope.$parent.dash.admin;
    let params = {
      params:{

      }
    };

    if(admin.role && admin.role.toUpperCase() == "ADMIN"){
      params.params.wilayah = admin.wilayah._id;
    }

    this.promise = this.Api.get('petugas',params).then(this.handleSuccess,this.handleError);
  }

  deletePetugas(event,id){
    this.id = id;
    this.event = "DELETE";
    this.Api.del('petugas/single/'+id).then(this.handleSuccess,this.handleError);
  }

  deleteSelectedPetugas(){
    if(this.selected.length>0){
        this.event = "DELETEALL";
        this.selected.forEach((item)=>{
            this.Api.del('petugas/single/'+item._id).then((response)=>{
            for(let i = 0;i<this.listpetugas.length;i++){
              if(this.listpetugas[i]._id == item._id){
                this.listpetugas.splice(i,1);
                this.$scope.$parent.dash.dataloader.petugas.data = this.listpetugas;
                break;
              }
            }
          },this.handleError);
        });
        this.ToastUtil.showSimpleToast("Petugas berhasil dihapus");
        this.selected = [];
    }
  }

  closeFilter(){
    this.query.filter = '';
    this.filter.show = !this.filter.show;
    this.tmplistpetugas = this.listpetugas;
  }

  onSearchChange(search){
     this.tmplistpetugas= this.$filter('filter')(this.listpetugas,search);
     this.promise = this.$timeout(()=>{

     },2000);
  }

  openNewPetugas(event){
    this.AppUtil.setVariable(0,this.$scope.$parent.dash.dataloader.wilayah.data);
    this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"NewPetugasController",controllerAs:"newpetugas",template:require('../../view/dialog/add-petugas.dialog.html')});

  }

  openDetailPetugas(event,item){
    if(this.selected.length>0){
      this.AppUtil.setVariable(0,this.selected[0]);
    }
    if(item){
      this.AppUtil.setVariable(0,item);
    }
    if(this.$scope.$parent.dash.dataloader.tps.status == false){
      this.Api.get('tps').then((response)=>{
        if(response.data && !response.data.error){
          this.$scope.$parent.dash.dataloader.tps.data = response.data.data;
          this.$scope.$parent.dash.dataloader.tps.status = true;
          this.AppUtil.setVariable(1,response.data.data);
          this.AppUtil.setVariable(2,this.$scope.$parent.dash.dataloader.wilayah.data);
          this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"DetailPetugasController",controllerAs:"profile",template:require('../../view/dialog/detail.petugas.html')});
        }
      },this.handleError);
    }else{
      this.AppUtil.setVariable(1,this.$scope.$parent.dash.dataloader.tps.data);
      this.AppUtil.setVariable(2,this.$scope.$parent.dash.dataloader.wilayah.data);
      this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"DetailPetugasController",controllerAs:"profile",template:require('../../view/dialog/detail.petugas.html')});
    }
    this.selected = [];
  }
}
