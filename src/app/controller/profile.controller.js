/**
 * profile dialog controller
 */

import {STORE_NAME,STORE_TOKEN} from './../config';

 export default class ProfileController {
   constructor(AppUtil,Api){
     this.event = null;
     this.AppUtil = AppUtil;
    this.ErrorUtil = AppUtil.getErrorUtil();
     this.Api = Api;
     this.$state = AppUtil.getState();
     this.progress = AppUtil.createProgress();
     this.user = AppUtil.getStore(STORE_NAME);
     if(this.user.role){
         this.user.role=this.user.role.toLowerCase();
     }
     this.ToastUtil = AppUtil.getToastUtil();
     this.myedit = {};
     this.edit = Object.assign({},this.user);

     this.handleSuccess = (response) =>{
        if(response.data){
          if(response.data.error){
              this.progress.reset();
              if(response.data.code == "UNKNOWN_USER"){
                 this.ToastUtil.showSimpleToast("Sudah digunakan,coba yang lain");
                 return;
              }
              this.ErrorUtil.checkError(this.event,response.data.code,this.ToastUtil);
              return;
          }
            this.progress.complete();
          if(this.event == "RESET"){
            this.ToastUtil.showSimpleToast("Berhasil,Periksa alamat email anda");
            return;
          }
          if(this.event == "DELETE"){
            this.AppUtil.removeStorage(STORE_NAME);
            this.AppUtil.removeStorage(STORE_TOKEN);
            this.$state.go('login');
            return;
          }
          this.AppUtil.saveStore(STORE_NAME,response.data.data);
          this.user = AppUtil.getStore(STORE_NAME);
          this.edit = AppUtil.getStore(STORE_NAME);
          this.progress.complete();
        }
     }

     this.handleError = (error) => {
           this.progress.reset();
           this.ErrorUtil.checkErrorByCodeNumber(this.event,error.status,this.$state,this.AppUtil.removeStorage);
     }
   }

     isAdmin(){
       if(!this.user.role){
           return false;
       }
       return this.user.role.toUpperCase() == "ADMIN";
     }

   deleteAccount(event){
     if(this.user.todeleted == this.user.username){
        this.progress.start();
       this.event = "DELETE";
       this.Api.del('admin/delete').then(this.handleSuccess,this.handleError);
     }
   }

   updateName(event){
     this.event = event;
     if(this.myedit.name == false){
       if(!this.edit.name ||  this.edit.name == this.user.name ){
         this.edit.name = this.user.name;
         return;
       }
       this.progress.start();
       this.Api.put('admin/update',{name:this.edit.name}).then(this.handleSuccess,this.handleError);
     }
   }

   updateUsername(event){
     this.event = event;
     if(this.myedit.username == false){
       if(!this.edit.username ||  this.edit.username == this.user.username ){
         this.edit.username = this.user.username;
         return;
       }
       if(this.edit.username.length < 6){
           this.ToastUtil.showSimpleToast("Username terlalu pendek");
         return;
       }
       this.progress.start();
       this.Api.put('admin/update',{username:this.edit.username}).then(this.handleSuccess,this.handleError);
     }
   }

   updatePassword(event){
      this.event = event;
      if(this.myedit.password == false){
        if(!this.edit.password){
          return;
        }
        if(this.edit.password.length < 5){
          this.ToastUtil.showSimpleToast("Kata sandi terlalu pendek");
          return;
        }
        this.progress.start();
        this.Api.put('admin/update',{password:this.edit.password}).then(this.handleSuccess,this.handleError);
      }
   }



   resetPassword(event){
     this.event = "RESET";
     this.progress.start();
     this.Api.post('admin/forget',{email:this.user.email}).then(this.handleSuccess,this.handleError);
   }

   updateEmail(event){
     this.event = event;
     if(this.myedit.email == false){
       if( this.edit.email == this.user.email ){
         this.edit.email = this.user.email;
         return;
       }
       if(!this.edit.email){
         this.edit.email = this.user.email;
          this.ToastUtil.showSimpleToast("Email tidak valid");
         return;
       }
       if(!this.AppUtil.validateEmail(this.edit.email)){
         this.ToastUtil.showSimpleToast("Email tidak valid");
         return;
       }
       this.progress.start();
       this.Api.put('admin/update',{email:this.edit.email}).then(this.handleSuccess,this.handleError);
     }
   }

   close(){
     this.AppUtil.getDialogUtil().cancel();
   }
 }
