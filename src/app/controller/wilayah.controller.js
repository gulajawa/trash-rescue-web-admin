/**
 * Created by muharizals on 12/05/2017.
 */
export default class WilayahController{
    constructor(Api,AppUtil,$rootScope){
        this.AppUtil = AppUtil;
        this.Api = Api;
        this.$rootScope = $rootScope;
        this.ToastUtil = AppUtil.getToastUtil();
        this.listwilayah = AppUtil.getVariable(0);
        this.myedit = {};

        this.handleError = (error) =>{
            this.ToastUtil.showSimpleToast("Terjadi kesalahan");
        };


    }

    fetchWilayah(){
        this.Api.get('wilayah').then((response)=>{
            this.listwilayah = response.data.data;
            this.$rootScope.$emit('dataemit:wilayah',response.data.data);
        },this.handleError);
    }


    close(){
        this.AppUtil.getDialogUtil().cancel();
    }

    tambahWilayah(){
        if(!this.myedit.name){
            this.ToastUtil.showSimpleToast("Masukkan nama wilayah");
            return;
        }
        this.Api.post('wilayah',this.myedit).then((response)=>{
            this.myedit.name = "";
            this.fetchWilayah();
        },this.handleError);
    }

    hapusWilayah(id){
        this.Api.del('wilayah/'+id).then((response)=>{
            this.ToastUtil.showSimpleToast("Item dihapus");
            this.fetchWilayah();
        },this.handleError);
    }

    editWilayah(id,name){
        if(name == "" || name == undefined || name == null){
            this.ToastUtil.showSimpleToast("Masukkan nama wilayah");
            return;
        }
        this.Api.put('wilayah/'+id,{name:name}).then((response)=>{
            this.ToastUtil.showSimpleToast("Item diperbarui");
            this.fetchWilayah();
        },this.handleError);
    }
}