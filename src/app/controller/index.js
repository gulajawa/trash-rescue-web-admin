import angular from 'angular';
import LoginController from './login.controller';
import DashController from './dashboard.controller';
import ProfileController from './profile.controller';
import DataAdminController from './data/admin.controller';
import DataDashController from './data/data.dash.controller';
import DataTpsController from './data/data.tps.controller';
import DataSensorController from './data/data.sensor.controller';
import DataPetugasController from './data/data.petugas.controller';
import NewAdminController from './data/new.admin.controller';
import NewPetugasController from './data/new.petugas.controller';
import NewTpsController from './data/new.tps.controller';
import NewContainerControler from './data/new.container.controller';
import DetailPetugasController from './data/detail.petugas.controller';
import DetailTpsController from './data/detail.tps.controller';
import TentangController from './tentang.controller';
//analytics
import StatistikController from './analytics/statistik.controller';

import WilayahController from './wilayah.controller';
import NewSensorController from './data/new.sensor.controller';
import PengaturanController from './pengaturan.controller';

//live

import TPSLiveController from './live/tpslive.controller';

const MODULE_NAME = "app.controller";

angular.module(MODULE_NAME,[])
  .controller('LoginController',['Api','AppUtil','$timeout',LoginController])
  .controller('DashController',['Api','AppUtil','$rootScope',DashController])
  .controller('ProfileController',['AppUtil','Api',ProfileController])
  .controller('DataDashController',['AppUtil','Api','$scope',DataDashController])
  .controller('DataTpsController',['Api','$filter','$timeout','AppUtil','$scope',DataTpsController])
  .controller('NewTpsController',['AppUtil','Api','$rootScope',NewTpsController])
  .controller('DetailTpsController',['$stateParams','AppUtil','Api','$filter','$timeout','$rootScope',DetailTpsController])
  .controller('DataSensorController',['Api','$filter','$timeout','AppUtil','$scope',DataSensorController])
  .controller('DataPetugasController',['Api','$filter','$timeout','AppUtil','$scope',DataPetugasController])
  .controller('NewPetugasController',['AppUtil','Api','$rootScope',NewPetugasController])
  .controller('NewContainerControler',['AppUtil','Api','$rootScope',NewContainerControler])
  .controller('DetailPetugasController',['AppUtil','Api',DetailPetugasController])
  .controller('DataAdminController',['Api','$filter','$timeout','AppUtil','$scope',DataAdminController])
  .controller('NewAdminController',['AppUtil','Api','$rootScope',NewAdminController])
  .controller('NewSensorController',['AppUtil','Api','$rootScope',NewSensorController])
  //live
  .controller('TPSLiveController',['AppUtil','Api','Socket','$scope','NgMap',TPSLiveController])
  .controller('TentangContoller', ['AppUtil', TentangController])
  .controller('WilayahController',['Api','AppUtil','$rootScope',WilayahController])
  .controller('PengaturanController',['AppUtil','Api','$rootScope',PengaturanController])
//analytics
    .controller('StatistikController',['AppUtil','Api','$scope','$timeout',StatistikController]);
export default MODULE_NAME;
