/**
 * Created by muharizals on 13/05/2017.
 */
import {STORE_NAME} from './../config';

export default class PengaturanController{
    constructor(AppUtil,Api,$rootScope) {
        this.AppUtil = AppUtil;
        this.Api = Api;
        this.$rootScope = $rootScope;
        this.ToastUtil = AppUtil.getToastUtil();
        this.ErrorUtil = AppUtil.getErrorUtil();
        this.user = AppUtil.getStore(STORE_NAME);
        this.progress = AppUtil.createProgress();
        this.selected = {};
        this.password = null;

        this.handleError = (error) => {
            this.progress.reset();
            this.ToastUtil.showSimpleToast("Terjadi kesalahan");
        }
    }

    close(){
        this.AppUtil.getDialogUtil().cancel();
    }

    isdisabled(){
        return !this.selected.tps && !this.selected.sensor && !this.selected.angkut && !this.selected.petugas;

    }

    removeData(myparam){
        let params = {
            params:{

            }
        };
        if(this.user.role && this.user.role.toUpperCase() == "ADMIN"){
            params.params.wilayah = this.user.wilayah._id;
        }
        this.Api.del(myparam,params).then((response)=>{
            this.progress.complete();
            switch (myparam){
                case "tps/remove/all":
                    this.$rootScope.$emit('remove:tps',response.data.data);
                    this.ToastUtil.showSimpleToast("TPS berhasil dihapus");
                    break;
                case "petugas/all":
                    this.$rootScope.$emit('remove:petugas',response.data.data);
                    this.ToastUtil.showSimpleToast("Petugas berhasil dihapus");
                    break;
                case "sensor/all":
                    this.$rootScope.$emit('remove:sensor',response.data.data);
                    this.ToastUtil.showSimpleToast("Sensor berhasil dihapus");
                    break;
                case "angkut/remove/all":
                    this.$rootScope.$emit('remove:angkut',response.data.data);
                    this.ToastUtil.showSimpleToast("Data pengangkutan berhasil dihapus");
                    break;
            }
        },this.handleError);
    }

    authAdministrator(){
        this.progress.start();
        let body ={
            username:this.user.username,
            password:this.password
        };
        this.Api.post('admin/auth',body).then((response)=>{
            if(response.data.error){
                this.progress.reset();
                this.ErrorUtil.checkError(null,response.data.code,this.ToastUtil);
                return;
            }
            this.password = "";
            if(this.selected.tps){
                this.removeData('tps/remove/all');
            }
            if(this.selected.petugas){
                this.removeData('petugas/all');
            }
            if(this.selected.sensor){
                this.removeData('sensor/all');
            }
            if(this.selected.angkut){
                this.removeData('angkut/remove/all');
            }
        },this.handleError);
    }

    hapusData(){
        if(this.isdisabled()){
            return;
        }

        if(!this.password){
            this.ToastUtil.showSimpleToast("Masukkan kata sandi administrator");
            return;
        }
        this.authAdministrator();
    }

}