/**
*Dash Dashcontroller
*/

import {STORE_NAME,STORE_TOKEN} from './../config';

export default class DashController{
   constructor(Api,AppUtil,$rootScope){
     this.Api = Api;
     this.AppUtil = AppUtil;
     this.$state = AppUtil.getState();

     this.ToastUtil = AppUtil.getToastUtil();
     try{
        this.username = AppUtil.getStore(STORE_NAME).username;
     }catch(err){
         this.username =  "admin";
     }

     this.admin = AppUtil.getStore(STORE_NAME);

     this.dataloader = {
        dash:{data:[]},
        admin:{status:false,data:[]},
        petugas:{status:false,data:[]},
        sensor:{status:false,data:[]},
        tps:{status:false,data:[]},
        wilayah:{status:false,data:[]},
        statistik:{status:false,data:[],title:"Semua TPS",tmp:null,mode:null}
     };

     $rootScope.$on('dataemit:tps', (event, data) => {
       this.dataloader.tps.data.push(data)
     });
     $rootScope.$on('dataemit:admin', (event, data) => {
       this.dataloader.admin.data.push(data)
     });
     $rootScope.$on('dataemit:petugas',(event, data) =>{
       this.dataloader.petugas.data.push(data)
     });
       $rootScope.$on('dataemit:sensor',(event, data) =>{
           this.dataloader.sensor.data.push(data)
       });

     $rootScope.$on('dataemit:wilayah',(event,data)=>{
        this.dataloader.wilayah.data = data;
     });

       $rootScope.$on('remove:tps',(event,data)=>{
           this.dataloader.tps.data = [];
           this.dataloader.tps.status = false;
       });
       $rootScope.$on('remove:sensor',(event,data)=>{
           this.dataloader.sensor.data = [];
           this.dataloader.sensor.status = false;
       });
       $rootScope.$on('remove:petugas',(event,data)=>{
           this.dataloader.petugas.data = [];
           this.dataloader.petugas.status = false;
       });
       $rootScope.$on('remove:angkut',(event,data)=>{
           this.dataloader.statistik.data = [];
           this.dataloader.statistik.status = false;
           this.dataloader.statistik.tmp = null;
           this.dataloader.statistik.mode = null;
       });

     this.showleftmenu = {
       status:false,
       style:{right:'0'},
     };

     let tmp = localStorage.getItem("showLeftMenu");
     if(tmp != undefined && tmp != null){
         if(tmp == "true"){
             this.showleftmenu.status = true;
             this.showleftmenu.style = {right:'15%'};
         }else{
             this.showleftmenu.status =false;
             this.showleftmenu.style = {right:'0'};
         }
     }

     this.showmenu = true;
     if(this.dataloader.wilayah.status == false){
         this.fetchWilayah();
     }
   }

   fetchWilayah(){
       this.Api.get('wilayah').then((response)=>{
           if(response.data){
               this.dataloader.wilayah.status = true;
               this.dataloader.wilayah.data = response.data.data;
           }
       },(error)=>{
           this.ToastUtil.showSimpleToast("Gagal mengambil data");
       })
   }

   isSuperAdmin(){
       if(this.admin == null || this.admin == undefined ){
           this.signOut();
           return false;
       }
       return this.admin.role == "SUPERADMIN";

   }

   checkLeft(){
     this.showleftmenu.status = !this.showleftmenu.status;
     localStorage.setItem("showLeftMenu",this.showleftmenu.status);
     if(this.showleftmenu.status){
       this.showleftmenu.style = {right:'15%'};
     }else{
       this.showleftmenu.style = {right:'0'};
     }
   }

   openBantuan(){
       let url = "https://arizalsaputro.net/file/";
       if(this.isSuperAdmin()){
            url += "manual_super.pdf";
       }else{
            url += "manual.pdf";
       }
       this.AppUtil.getWindow().open(url,'_blank');
   }

   moveToAnotherPage(name,data){
    this.$state.go(name,data);
   }

   getTitleName(current){
     switch(current){
       case "index" : return "Halaman Utama";
       case "login" : return "Masuk";
       case "home" : return "Selamat Datang";
       case "home.data" : return "Dashboard";
       case "home.datatps" : return "Data Tps";
       case "home.datasensor": return "Data Sensor";
       case "home.datapetugas": return "Data Petugas";
       case "home.dataadmin":return "Data Administrator";
       case "home.detailtps": return "Detail";
       case "home.livetps" : return "Live";
         case "home.statistik" : return "Statistik Pengangkutan";
      default: return "App";
     }
   }

   signOut(){
     this.AppUtil.removeStorage(STORE_NAME);
     this.AppUtil.removeStorage(STORE_TOKEN);
     this.AppUtil.getState().go('login');
   }

   openProfile(event){
     this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"ProfileController",controllerAs:"profile",template:require('../view/dialog/profile.dialog.html')});
   }

   openTentang(event){
       this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"TentangContoller",controllerAs:"tentang",template:require('../view/dialog/tentang.dialog.html')});
   }

   openWilayah(event){
       this.AppUtil.setVariable(0,this.dataloader.wilayah.data);
       this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"WilayahController",controllerAs:"wilayah",template:require('../view/dialog/wilayah.dialog.html')});
   }

   openPengaturan(event){
       this.AppUtil.getDialogUtil().showCustomDialog(event,{controller:"PengaturanController",controllerAs:"pengaturan",template:require('../view/dialog/pengaturan.dialog.html')});
   }


   toggleLeft(){
     this.AppUtil.navigationToggle('left');
   }

   back(){
     window.history.back();
   }
}
