/**
 * Created by Aprilita on 4/25/2017.
 */

export default class TentangController{
    constructor(AppUtil){
        this.event = null;
        this.AppUtil = AppUtil;

    }

    close(){
        this.AppUtil.getDialogUtil().cancel();
    }
}
