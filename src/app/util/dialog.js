export default class DialogUtil{
  constructor($mdDialog,$q,$mdMedia,$mdBottomSheet){
    this.$mdDialog = $mdDialog;
    this.$q = $q;
    this.$mdMedia = $mdMedia;
    this.$mdBottomSheet = $mdBottomSheet;
  }

  showAlert(ev,data) {
         let that = this;
         this.$mdDialog.show(
           that.$mdDialog.alert()
             .parent(angular.element(document.querySelector('#popupContainer')))
             .clickOutsideToClose(true)
             .title(data.title)
             .textContent(data.content)
             .ariaLabel("Waspada")
             .ok('Mengerti!')
             .targetEvent(ev)
         );
  }

  showBottomGrid(data){
          this.$mdBottomSheet.show({
              templateUrl: data.template,
              controller: data.controller,
              clickOutsideToClose: data.clickable
          });
  }

  showPrompt(ev,data) {
          var def = this.$q.defer();
          var confirm = this.$mdDialog.prompt()
          .title(data.title)
          .textContent(data.content)
          .placeholder(data.placeholder)
          .ariaLabel(data.label)
          .ok('Oke')
          .cancel('batal');

         this.$mdDialog.show(confirm).then(function(result){
            def.resolve(result);
         },function(err){
           def.reject(err);
         });

         return def.promise;
  }

   showCustomDialog(ev,data){
      this.$mdDialog.show({
        controller: data.controller,
        controllerAs:data.controllerAs,
        template: data.template,
        parent: angular.element(document.body),
        targetEvent: ev,
        clickOutsideToClose:true
      });
   }

   showConfirm(ev,data) {
     var def = this.$q.defer();
     var confirm = this.$mdDialog.confirm()
           .title(data.title)
           .textContent(data.content)
           .ariaLabel(data.label)
           .targetEvent(ev)
           .ok('okay')
           .cancel('batal');
     this.$mdDialog.show(confirm).then(function() {
       def.resolve(true);
     }, function() {
       def.reject(false);
     });
     return def.promise;
   };

  cancel(){
    this.$mdDialog.cancel();
  }

  hide(){
    this.$mdDialog.hide();
  }

  static Create($mdDialog,$q,$mdMedia,$mdBottomSheet){
    return new DialogUtil($mdDialog,$q,$mdMedia,$mdBottomSheet);
  }
}
