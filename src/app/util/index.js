import angular from 'angular';

import AppUtil from './util';
import ToastUtil from './toast';
import DialogUtil from './dialog';
import ErrorUtil from './errorutil';
import focusMe from './focus';
import socket from './socket';

import AngularCrypto from './crypto/mdo-angular-cryptography';
const MODULE_NAME = "app.factorys";
import {SECRET_KEY} from './../config';

angular.module(MODULE_NAME,[AngularCrypto])
  .config(['$cryptoProvider',function($cryptoProvider){
    $cryptoProvider.setCryptographyKey(SECRET_KEY);
  }])
  .factory('Socket',[socket.Create])
  .factory('AppUtil',['ngProgressFactory','$cookies','$state','ErrorUtil','DialogUtil','ToastUtil','$mdSidenav','$crypto','$window','focus',AppUtil.Create])
  .factory('ToastUtil',['$mdToast',ToastUtil.Create])
  .factory('DialogUtil',['$mdDialog','$q','$mdMedia','$mdBottomSheet',DialogUtil.Create])
  .factory('ErrorUtil',['DialogUtil',ErrorUtil.Create])
  .factory('focus',['$timeout','$window',focusMe]);

export default MODULE_NAME;
