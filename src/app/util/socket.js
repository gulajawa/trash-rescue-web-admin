import io from 'socket.io-client';
import {BASE_URL} from '../config';

export default class Socket {
  static Create(){
    return new Socket();
  }
  constructor() {
    this.socket = io(BASE_URL);
    this.setupSoscket();
  }

  setupSoscket(){
    this.socket.on('connect',()=>{
      console.log('konek bosku');
    });

    this.socket.on('disconnect',()=>{
      console.log('disconnect bro');
    });

    this.socket.on('error',()=>{
      console.log('error bosku');
    });
  }

  getSocket(){
    return this.socket;
  }
}
