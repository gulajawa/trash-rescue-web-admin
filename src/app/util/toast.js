export default class ToastUtil{
  constructor($mdToast){
    this.$mdToast = $mdToast;
    this.last = {
      bottom: false,
      top: true,
      left: false,
      right: true
    };
    this.toastPosition = angular.extend({},this.last)
  }

  getToastPosition() {
    this.sanitizePosition();
    let that = this.toastPosition;
    return Object.keys(this.toastPosition)
      .filter(function(pos) {
         return that[pos];
      })
      .join(' ');
  };

  sanitizePosition() {
    let current = this.toastPosition;
    if ( current.bottom && this.last.top ) current.top = false;
    if ( current.top && this.last.bottom ) current.bottom = false;
    if ( current.right && this.last.left ) current.left = false;
    if ( current.left && this.last.right ) current.right = false;
    this.last = angular.extend({},current);
  }

  showSimpleToast(text,config) {
    if(config){
      this.last = config;
    }
    let pinTo = this.getToastPosition();
    this.$mdToast.show(
      this.$mdToast.simple()
        .textContent(text)
        .position(pinTo )
        .hideDelay(3000)
    );
  };

  static Create($mdToast){
    return new ToastUtil($mdToast);
  }

}
