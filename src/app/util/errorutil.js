import {STORE_NAME,STORE_TOKEN} from './../config';

export default class ErrorUtil{
    constructor(DialogUtil){
      this.DialogUtil = DialogUtil;
    }


    checkErrorByCodeNumber(ev,code,state,remover,Toast){
      if(code == 401){
        remover(STORE_NAME);remover(STORE_TOKEN);state.go('login');
        return;
      }
      let fail = {};
      fail.title = "Gagal";
      switch(code){
        case 405 : fail.content="Method tidak ditemukan";
                   break;
        case 404 : fail.content="Alamat tidak ditemukan";
                  break;
        case -1 :  fail.content="Terjadi kesalahan jaringan";
                  break;
        default : fail.content = "Terjadi kesalahan yang tidak diketahui";
                  break;
      }
      if(ev){
        this.DialogUtil.showAlert(ev,fail);
      }
      else if(Toast){
          Toast.showSimpleToast(fail.content);
      }
    }

    checkError(ev,error,Toast){
          let fail = {};
          fail.title = "Gagal";
          switch(error){
            case "MethodNotAllowedError" :	fail.content="Method tidak ditemukan";break;
            case "InvalidCredentials" :	fail.content="TOKEN tidak valid,atau token salah";break;
            case "ResourceNotFound" :	fail.content="Halaman Tidak ditemukan";break;
            case "PARAMETER_REQUIRED" :	fail.content="Lengkapi semua parameter";break;
            case "INVALID_USERNAME" :	fail.content="Username salah,periksa kembali username";break;
            case "INVALID_EMAIL_ADDRESS" :	fail.content="Alamat email salah,periksa kembali alamat email";break;
            case "INVALID_PASSWORD" :	fail.content="Kata sandi salah,periksa kembali kata sandi";break;
            case "INVALID_API_KEY" :	fail.content="Api Key tidak ditemukan";break;
            case "INVALID_SENSOR_ID" :	fail.content="Id TPS tidak ditemukan";break;
            case "INVALID_TPS_ID" :	fail.content="Id TPS tidak ditemukan";break;
            case "INVALID_PETUGAS_ID" :	fail.content="Id Petugas tidak ditemukan";break;
            case "PERMISSION_DENIED" :	fail.content="Tidak memiliki akses";break;
            case "UNKNOWN_USER" :	fail.content="User tidak ditemukan,periksa kembali email atau username anda";break;
            case "UNKNOWN_EMAIL" :	fail.content="Alamat email tidak ditemukan";break;
            case "UNKNOWN_USERNAME" :	fail.content="Username tidak ditemukan";break;
            case "UNKNOWN_ADDRESS" :	fail.content="Alamat tidak ditemukan";break;
            case "UNKNOWN_ERROR" :	fail.content="Terjadi kesalahan yang tidak diketahui";break;
            case "EMAIL_ALREADY_USE" :	fail.content="Alamat email sudah digunakan";break;
            case "USERNAME_ALREADY_USE" :	fail.content="Username sudah digunakan";break;
            case "PHONE_NUMBER_ALREADY_USE" :	fail.content="Nomor hp sudah digunakan";break;
            case "PASSWORD_TOO_SHORT" :	fail.content="Kata sandi terlalu pendek";break;
            default : fail.content = "Terjadi kesalahan yang tidak diketahui";break;
          }
          if(Toast){
              Toast.showSimpleToast(fail.content);
          }else{
            this.DialogUtil.showAlert(ev,fail);
          }
    }
    static Create(DialogUtil){
      return new ErrorUtil(DialogUtil);
    }
}
