export default class Util{

  static Create(ngProgressFactory,$cookies,$state,ErrorUtil,DialogUtil,ToastUtil,$mdSidenav,$crypto,$window,focus){
    return new Util(ngProgressFactory,$cookies,$state,ErrorUtil,DialogUtil,ToastUtil,$mdSidenav,$crypto,$window,focus);
  }

  constructor(ngProgressFactory,$cookies,$state,ErrorUtil,DialogUtil,ToastUtil,$mdSidenav,$crypto,$window,focus){
    this.ngProgressFactory = ngProgressFactory;
    this.$cookies = $cookies;
    this.$state = $state;
    this.ErrorUtil = ErrorUtil;
    this.DialogUtil = DialogUtil;
    this.ToastUtil = ToastUtil;
    this.$mdSidenav = $mdSidenav;
    this.$crypto = $crypto;
    this.$window = $window;
    this.focus = focus;
    this.myvariable = [];
  }

  getVariable(index){
    return this.myvariable[index];
  }
  setVariable(index,value){
    this.myvariable[index] = value;
  }

  getFocus(){
    return this.focus;
  }

  getWindow(){
    return this.$window;
  }

  getCookies(){
    return this.$cookies;
  }

  getState(){
    return this.$state;
  }

  getErrorUtil(){
    return this.ErrorUtil;
  }

  getDialogUtil(){
    return this.DialogUtil;
  }

  getToastUtil(){
    return this.ToastUtil;
  }

  setFocus(id){
    let elm = this.$window.document.getElementById(id);
    if(elm){
      elm.focus();
    }
  }

  navigationToggle(position){
    return this.$mdSidenav(position).toggle();
  }

  saveStore(key,obj){
      localStorage.setItem(key,this.$crypto.encrypt(JSON.stringify(obj)));
  }

  getStore(key){
     let todecrypt = localStorage.getItem(key);
     if(todecrypt){
       let result = null;
       try{
          result = this.$crypto.decrypt(todecrypt);
          return JSON.parse(result);
       } catch(error){
         console.log('gagal');
         return null;
       }

     }else{
       return null;
     }
  }

  removeStorage(key){
    localStorage.removeItem(key);
  }

  createProgress(){
    let progress= this.ngProgressFactory.createInstance();
    progress.setColor('#FFEB3B');
    progress.setHeight('3px');
    return progress;
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}
