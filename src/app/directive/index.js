import angular from 'angular';

import updatetitle from './update-title';
import eventFocus from './eventFocus';
import ngEnter from './ngEnter';
import clickAnywhereButHere from './click-but-not-here';

const MODULE_NAME = "app.directives";

angular.module(MODULE_NAME,[])
.directive('updateTitle',['$rootScope','$timeout',updatetitle])
.directive('eventFocus', ['focus',eventFocus])
.directive('clickAnywhereButHere',['$document',clickAnywhereButHere])
.directive('ngEnter', ngEnter);

export default MODULE_NAME;
